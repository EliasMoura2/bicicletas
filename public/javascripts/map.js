var map = L.map('main_map').setView([-27.3828050278, -55.9495353433 ], 13)
L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
  attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
}).addTo(map);


// L.marker([-27.37280502782093, -55.8959353433769]).addTo(map)
// L.marker([-27.38280502782093, -55.9275353433769]).addTo(map)
// L.marker([-27.38280502782093, -55.9475353433769]).addTo(map)


  $.ajax({
    dataType: 'json',
    url: 'http://localhost:4000/api/v1/bicicletas',
    success: function(result){
      // console.log(result)
      result.bicicletas.forEach(function(bici){
        L.marker(bici.ubicacion, {title: bici.code}).addTo(map)
        .bindPopup(`lat: ${bici.ubicacion[0]} long: ${bici.ubicacion[1]}`)
        .openPopup();
        // console.log(bici.ubicacion)
      })
    }
  })
