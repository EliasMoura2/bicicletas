const mongoose = require('mongoose');
const Bicicleta = require('../../models/Bicicleta')
const server = require('../../bin/www')
const request = require('request')

const base_url = 'mongodb://localhost:3000/api/v1/bicicletas'

describe('Bicicleta API', () => {
  beforeEach(function(done){
    const URI = 'mongodb://localhost/testdb'
    mongoose.connect(URI, {
      useNewUrlParser: true,
      useUnifiedTopology: true
    })

    const db = mongoose.connection
    db.on('error', console.error.bind(console, 'connection error'))
    db.once('open', function(){
      console.log('We are connected to test database!')
      done()
    })
  })

  afterEach(function(done){
    Bicicleta.deleteMany({}, function(err, success){
      if(err) console.log(err)
      done()
    })
  })

  describe('GET Bicicletas /', () => {
    it('status 200', (done) => {
      request.get(base_url, (err, res, body) => {
        var result = JSON.parse(body)
        expect(res.statusCode).toBe(200)
        expect(result.bicicletas.length).toBe(0)
        done()
      })
    })
  })

  describe('POST bicicletas /create', () => {
    it('status 200', (done) => {
      var headers = { "Content-type": "application/json" }
      var aBici = '{"code": 10, "color": "rojo", "modelo": "urbano", "lat": -27.37280502, "long": -55.8959353}'
      request.post({
          headers: headers,
          url: base_url + '/create',
          body: aBici
        }, function( err, res, body){

          expect(res.statusCode).toBe(200)
          var bici = JSON.parse(body).bicicleta
          console.log(bici)
          expect(bici.color).toBe("rojo")
          expect(bici.ubicacion[0]).toBe(-27.37280502)
          expect(bici.ubicacion[1]).toBe(-55.8959353)
          done()
        }
      )
    })
  })
})

