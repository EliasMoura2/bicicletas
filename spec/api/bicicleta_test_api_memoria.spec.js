const Bicicleta = require('../../models/BicicletaMemoria')
const request = require('request')
const server = require('../../bin/www')

describe('Bicicleta API', () => {
  describe('GET Bicicletas /', () => {
    it('status 200', () => {
      expect(Bicicleta.allBicis.length).toBe(0)

      let a = new Bicicleta(1, 'Gris', 'urbano', [-27.37280502782093, -55.8959353433769])
      Bicicleta.add(a)

      request.get('http://localhost:3000/api/v1/bicicletas', (err, res, body) => {
        expect(res.statusCode).toBe(200)
      })
    })
  })

  // describe('POST Bicicletas/api/v1/create', () => {
  //   it('status 200', (done) => {
  //     var headers = { "Content-type": "application/json" }
  //     var aBici = '{"id": 10, "color": "rojo", "modelo": "urbano", "lat": -27.37280502, "long": -55.8959353}'
  //     request.post({
  //         headers: headers,
  //         url: 'http://localhost:3000/api/v1/bicicletas/create',
  //         body: aBici
  //       }, function( err, res, body){
  //         expect(res.statusCode).toBe(200)
  //         expect(Bicicleta.findById(10).color).toBe("rojo")
  //         done()
  //       }
  //     )
  //   })
  // })

  // describe('POST',()=>{
  //   it('status 200', () => {
  //     spyOn(request, 'post');

  //     request.post({"id": 10, "color": "rojo", "modelo": "urbano", "lat": -27.37280502, "long": -55.8959353});

  //     expect(request.post).toHaveBeenCalledWith({ id: 10, color: "rojo", modelo: "urbano", lat: -27.37280502, long: -55.8959353});
  //   })
  // })

})