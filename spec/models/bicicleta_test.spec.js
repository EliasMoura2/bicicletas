const mongoose = require('mongoose')
var Bicicleta = require('../../models/Bicicleta')

describe('Testing Bicicletas', function(){
  beforeEach(function(done){
    const URI = 'mongodb://localhost/testdb'
    mongoose.connect(URI, {
      useNewUrlParser: true,
      useUnifiedTopology: true
    })

    const db = mongoose.connection
    db.on('error', console.error.bind(console, 'connection error'))
    db.once('open', function(){
      console.log('We are connected to test database!')
      done()
    })
  })

  afterEach(function(done){
    Bicicleta.deleteMany({}, function(err, success){
      if(err) console.log(err)
      done()
    })
  })
  describe('Bicicleta.createInstance', () => {
    it('crea una instancia de Bicicleta', () => {
      var aBici  = Bicicleta.createInstance(1, "Azul", "Urbana", ["-27.8", "-55.7"])

      expect(aBici.code).toBe(1)
      expect(aBici.color).toBe("Azul")
      expect(aBici.modelo).toBe("Urbana")
      expect(aBici.ubicacion[0]).toEqual(-27.8)
      expect(aBici.ubicacion[1]).toEqual(-55.7)
    })
  })

  describe('Bicicleta.allBicis', () => {
    it('comienza vacia', (done) => {
      Bicicleta.allBicis(function(err, bicis){
        expect(bicis.length).toBe(0)
        done()
      })
    })
  })

  describe('Bicicleta.add', () => {
    it('agrega solo una bici', (done) => {
      var aBici  = new Bicicleta({code: 1, color: "Azul", modelo: "Urbana"})

      Bicicleta.add(aBici, function(err, newBici){
        if(err) console.log(err)

        Bicicleta.allBicis(function(err, bicis){
          expect(bicis.length).toEqual(1)
          expect(bicis[0].code).toEqual(aBici.code)

          done()
        })
      })

    })
  })

  describe('Bicicleta.findByCode', () => {
    it('debe devolver la bici con code 1', (done) => {
      Bicicleta.allBicis(function( err, bicis){
        expect(bicis.length).toBe(0)

        var aBici  = new Bicicleta({code: 1, color: "Azul", modelo: "Urbana"})
        Bicicleta.add(aBici, function(err, newBici){
          if(err) console.log(err)

          var aBici2  = new Bicicleta({code: 2, color: "Verde", modelo: "Carrera"})
          Bicicleta.add(aBici2, function(err, newBici){
            if(err) console.log(err)

            Bicicleta.findByCode(1, function(err, targetBici){
              expect(targetBici.code).toBe(aBici.code)
              expect(targetBici.color).toBe(aBici.color)
              expect(targetBici.modelo).toBe(aBici.modelo)

              done()
            })
          })
        })
      })
    })
  })

})

// beforeEach(() => { Bicicleta.allBicis = []; })

// describe('Bicicleta.allBicis', () => {
//   it('comienza vacia', () => {
//     expect(Bicicleta.allBicis.length).toBe(0)
//   })
// })

// describe('Bicicleta.add', () => {
//   it('Agregamos una', () => {
//     // precondition
//     expect(Bicicleta.allBicis.length).toBe(0)

//     let a = new Bicicleta(1, 'rojo', 'urbano', [-27.37280502782093, -55.8959353433769])

//     Bicicleta.add(a);

//     // postcondition
//     expect(Bicicleta.allBicis.length).toBe(1)

//     // que la primer posicion del array contenga la bici agregada
//     expect(Bicicleta.allBicis[0]).toBe(a)
//   })
// })

// describe('Bicicleta.findById', () => {
//   it('devolver la bici con el id buscado', () => {
//     // expect(Bicicleta.allBicis.length).toBe(0)
//     // al tenerlo aca falla porque el test anterior creo una bici, deberiamos iniciar el array de bicis en cero
//     // Bicicleta.allBicis = [];

//     let aBici = new Bicicleta(1, 'Azul', 'carrera')
//     let aBici2 = new Bicicleta(2, 'rojo', 'urbano')

//     Bicicleta.add(aBici);
//     Bicicleta.add(aBici2);

//     let targetBici = Bicicleta.findById(1)

//     expect(targetBici.id).toBe(1)
//     expect(targetBici.color).toBe(aBici.color)
//     expect(targetBici.modelo).toBe(aBici.modelo)

//   })
// })

// describe('Bicicleta.removeById', () => {
//   it('eliminar la bici con el id buscado', () => {
//     expect(Bicicleta.allBicis.length).toBe(0)

//     let a = new Bicicleta(1, 'rojo', 'urbano', [-27.37280502782093, -55.8959353433769])

//     Bicicleta.add(a);

//     expect(Bicicleta.allBicis.length).toBe(1)

//     Bicicleta.removeById(1)

//     expect(Bicicleta.allBicis.length).toBe(0)
//   })
// })