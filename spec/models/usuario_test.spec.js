const mongoose = require('mongoose')
const Bicicleta = require('../../models/Bicicleta')
var Reserva = require('../../models/Reserva')
const Usuario = require('../../models/Usuario')

describe('Testing Usuarios', function(){
  beforeEach(function(done){
    var URI = 'mongodb://localhost/testdb'

    mongoose.connect(URI, {
      useNewUrlParser: true,
      useUnifiedTopology: true
    })

    const db = mongoose.connection
    db.on('error', console.error.bind(console, 'connection error'))
    db.once('open', function(){
      console.log('We are connected to test database')

      done()
    })
  })

  afterEach(function(done){
    Reserva.deleteMany({}, function(err, success){
      if(err) console.log(err)
        mongoose.disconnect(err);
        done()
      Usuario.deleteMany({}, function(err, success){
        if(err) console.log(err)
          mongoose.disconnect(err);
          done()
        Bicicleta.deleteMany({}, function(err, success){
          if(err) console.log(err)
          mongoose.disconnect(err); 
          done()
        })
      })
    })
  })

  describe('Cuando un Usuario reserva una bici', () => {
    it('debe existir la reserva', (done) => {
      const usuario = new Usuario({nombre: 'Elias'})

      usuario.save()

      const bicicleta = new Bicicleta({code: 10, color: "rojo", modelo: "urbano"})
      bicicleta.save()

      const today = new Date()
      const yesterday = new Date()

      yesterday.setDate(today.getDate() + 1)

      usuario.reservar(bicicleta._id, today, yesterday, function(err, reserva){
        Reserva.find({}).populate('bicicleta').populate('usuario').exec(function(err, reservas){
          console.log(reservas[0])
          expect(reservas.length).toBe(1)
          expect(reservas[0].diasDeReserva()).toBe(2)
          expect(reservas[0].bicicleta.code).toBe(1)
          expect(reservas[0].usuario.nombre).toBe(usuario.nombre)

          done()
        })
      })
    })
  })
})