var Bicicleta = require('../../models/BicicletaMemoria')

beforeEach(() => { Bicicleta.allBicis = []; })

describe('Bicicleta.allBicis', () => {
  it('comienza vacia', () => {
    expect(Bicicleta.allBicis.length).toBe(0)
  })
})

describe('Bicicleta.add', () => {
  it('Agregamos una', () => {
    // precondition
    expect(Bicicleta.allBicis.length).toBe(0)

    let a = new Bicicleta(1, 'rojo', 'urbano', [-27.37280502782093, -55.8959353433769])

    Bicicleta.add(a);

    // postcondition
    expect(Bicicleta.allBicis.length).toBe(1)

    // que la primer posicion del array contenga la bici agregada
    expect(Bicicleta.allBicis[0]).toBe(a)
  })
})

describe('Bicicleta.findById', () => {
  it('devolver la bici con el id buscado', () => {
    // expect(Bicicleta.allBicis.length).toBe(0)
    // al tenerlo aca falla porque el test anterior creo una bici, deberiamos iniciar el array de bicis en cero
    // Bicicleta.allBicis = [];

    let aBici = new Bicicleta(1, 'Azul', 'carrera')
    let aBici2 = new Bicicleta(2, 'rojo', 'urbano')

    Bicicleta.add(aBici);
    Bicicleta.add(aBici2);

    let targetBici = Bicicleta.findById(1)

    expect(targetBici.id).toBe(1)
    expect(targetBici.color).toBe(aBici.color)
    expect(targetBici.modelo).toBe(aBici.modelo)

  })
})

describe('Bicicleta.removeById', () => {
  it('eliminar la bici con el id buscado', () => {
    expect(Bicicleta.allBicis.length).toBe(0)

    let a = new Bicicleta(1, 'rojo', 'urbano', [-27.37280502782093, -55.8959353433769])

    Bicicleta.add(a);

    expect(Bicicleta.allBicis.length).toBe(1)

    Bicicleta.removeById(1)

    expect(Bicicleta.allBicis.length).toBe(0)
  })
})