const { Schema, model } = require('mongoose');
const moment = require('moment');

var reservaSchema = new Schema({
  desde: Date,
  hasta: Date,
  bicicleta: { type: Schema.Types.ObjectId, ref: 'Bicicleta' },
  usuario: { type: Schema.Types.ObjectId, ref: 'Usuario'}
})
// bicicleta y usuarios son referencias

reservaSchema.method('diasDeReserva', function(){
  return moment(this.hasta).diff(moment(this.desde), 'days') + 1;
})

module.exports = model('Reserva', reservaSchema)