const mongoose = require('mongoose');
const Schema = mongoose.Schema
const model = mongoose.model

const tokenSchema = new Schema ({
  _userId: {
    type: mongoose.Schema.Types.ObjectId,
    required: true,
    ref: 'Usuario'
  },
  token: {
    type: String,
    required: true
  },
  createAt: {
    type: Date,
    required: true,
    default: Date.now,
    expires: 43200
  }
})

module.exports = model('Token', tokenSchema)