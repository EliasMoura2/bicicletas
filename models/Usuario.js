const { Schema, model} = require('mongoose');
const uniqueValidator = require('mongoose-unique-validator')
const bcrypt = require('bcrypt')
const crypto = require('crypto');
const Reserva = require('../models/Reserva')
const Token = require('../models/Token')
const mailer = require('../mailer/mailer')

const saltRounds = 10;

const validateEmail = function(email) {
  const re = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
  return re.test(email)
}

var usuarioSchema = new Schema({
  nombre: {
    type: String,
    trim: true,
    required: [true, 'El nombre es obligatorio']
  },
  email: {
    type: String,
    trim: true,
    required: [true, 'El email es obligatorio'],
    unique: true,
    lowercase: true,
    validate: [validateEmail, 'Ingrese un email valido'],
    // match: [/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/]
  },
  password: {
    type: String,
    trim: true,
    required: [true, 'La password es obligatorio'],
  },
  passwordResetToken: String,
  passwordResetTokenExpires: Date,
  verificado: {
    type: Boolean,
    default: false
  },
  googleId: String,
  facebookId: String
})

// agregamos el uniqueValidator como plugin
usuarioSchema.plugin(uniqueValidator, {message: 'El {PATH} el email ya se encuentra registrado por otro usuario'})

// funcion que se ejecuta antes de un save (hashea la password)
usuarioSchema.pre('save', function(next){
  if(this.isModified('password')){
    this.password = bcrypt.hashSync(this.password, saltRounds)
  }
  next()
})

usuarioSchema.method('validPassword', function(password){
  return bcrypt.compareSync(password, this.password)
})

usuarioSchema.method('reservar', function(biciId, desde, hasta, cb){
  var reserva = new Reserva ({ usuario: this._id, bicicleta: biciId, desde: desde, hasta: hasta})
  console.log(reserva)
  reserva.save(cb)
})

  usuarioSchema.method('enviar_email_bienvenida', function(cb){
    const token = new Token({_userId: this.id, token: crypto.randomBytes(16).toString('hex')})
    const email_destination = this.email
    token.save(function (err){
      if(err) { return console.log(err.message) }

      // configuracion email
      const mailOptions = {
        from: 'no-reply@eliasmoura.com',
        to: email_destination,
        subject: 'Verificacion de cuenta',
        text: 'Hola,\n\n' + 'Por favor, para verificar su cuenta haga click en este link: \n' +'http://localhost:3000'+'\/token/confirmation\/'+token.token+'.\n'
      }

      mailer.sendMail(mailOptions, function(err) {
        if(err) { return console.log(err.message) }

        console.log('A verification email has been to sent to ' + email_destination +'.')
        // Se ha enviado un email de bienvenida a
      })
    })
  })

  usuarioSchema.method('resetPassword', function(cb){
    const token = new Token({_userId: this.id, token: crypto.randomBytes(16).toString('hex')})
    const email_destination = this.email
    token.save(function(err){
      if(err) { return cb(err)}

      // configuracion email
      const mailOptions = {
        from: 'no-reply@eliasmoura.com',
        to: email_destination,
        subject: 'Reseteo de password',
        text: 'Hola,\n\n' + 'Por favor, para resetear la password de su cuenta haga click en este link: \n' +'http://localhost:3000'+'\/resetPassword\/'+token.token+'.\n'
      }

      mailer.sendMail(mailOptions, function(err) {
        if(err) { return cb(err) }

        console.log('Se envio un email para resetear la password a: ' + email_destination +'.')
        // Se ha enviado un email de bienvenida a
      })

      cb(null)
    })
  })
/* 
const schema = kittySchema = new Schema(..);
schema.method('meow', function () {
  console.log('meeeeeoooooooooooow');
})
const Kitty = mongoose.model('Kitty', schema);
const fizz = new Kitty;
fizz.meow(); // meeeeeooooooooooooow
*/

  usuarioSchema.statics.findOrCreateByGoogle = function findOrCreate(condition, callback){
    const self = this
    // se referencia al this porqeu cuando estemos dentro del callback de la busqueda se pierde la referencia del usuario
    self.findOne({
      $or: [
        {'googleId': condition.id}
      ]},
      (err, result) => {
        if(result){
          callback(err, result)
        } else {
          console.log(' -------------CONDITION-------------')
          console.log(condition)
          let values = {}
          values.googleId = condition.id
          values.email = process.env.GMAIL
          values.nombre = condition.displayName || 'SIN NOMBRE'
          values.verificado = true
          values.password = process.env.GOOGLE_PASS
          console.log(' --------------VALUES---------------')
          console.log(values)
          self.create(values, (err, result) => {
            if(err) { console.log(err) }
            return callback(err, result)
          })
        }
      }
    )
  }

  usuarioSchema.statics.findOneOrCreateByFacebook = function findOneOrCreate(condition, callback){
    const self = this
    // se referencia al this porqeu cuando estemos dentro del callback de la busqueda se pierde la referencia del usuario
    self.findOne({
      $or: [
        {'facebookId': condition.id}
      ]},
      (err, result) => {
        if(result){
          callback(err, result)
        } else {
          console.log(' -------------CONDITION-------------')
          console.log(condition)
          let values = {}
          values.facebookId = condition.id
          values.email = 'elpepe@gmail.com'
          values.nombre = condition.displayName || 'SIN NOMBRE'
          values.verificado = true
          values.password = 'algo'
          console.log(' --------------VALUES---------------')
          console.log(values)
          self.create(values, (err, result) => {
            if(err) { console.log(err) }
            return callback(err, result)
          })
        }
      }
    )
  }


module.exports = model('Usuario', usuarioSchema)