var Bicicleta = function (id, color, modelo, ubicacion) {
  this.id = id
  this.color = color
  this.modelo = modelo
  this.ubicacion = ubicacion
}

Bicicleta.prototype.toString = function (){
  return 'id: ' + this.id + ' | color: ' + this.color
}

Bicicleta.allBicis = [];

Bicicleta.add = function(aBici){
  Bicicleta.allBicis.push(aBici)
}

// let a = new Bicicleta(1, 'rojo', 'urbano', [-27.37280502782093, -55.8959353433769])
// let b = new Bicicleta(2, 'Azul', 'urbano', [-27.38280502782093, -55.9275353433769])

// Bicicleta.add(a);
// Bicicleta.add(b);

Bicicleta.findById = function(aBiciId){
  var aBici = Bicicleta.allBicis.find( x => x.id == aBiciId)
  if(aBici){
    return aBici
  } else {
    throw new Error(`No existe una bicicleta con el id: ${aBiciId}`)
  }
}

Bicicleta.removeById = function(aBiciId){
  // var aBici = Bicicleta.findById(aBiciId)
  for (let i = 0; i < Bicicleta.allBicis.length; i++) {
    if(Bicicleta.allBicis[i].id == aBiciId){
      Bicicleta.allBicis.splice(i, 1)
      break
    }
    
  }
}

module.exports = Bicicleta