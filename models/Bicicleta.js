const mongoose = require('mongoose')
const Schema = mongoose.Schema
const model = mongoose.model
// const { Schema, model } = require('mongoose') 

const bicicletaSchema = new Schema({
  code: Number,
  color: String,
  modelo: String,
  ubicacion: {
    type:[Number], index: {type:'2dphere', sparse:true}
  }
})

// bicicletaSchema.static('createInstance', function(code, color, modelo, lat, long){
//   return new this({
//     code: code,
//     color: color,
//     modelo: modelo,
//     ubicacion: {
//       lat: lat,
//       long: long
//     }
//   })
// })

bicicletaSchema.static('createInstance', function(code, color, modelo, ubicacion){
  return new this({
    code: code,
    color: color,
    modelo: modelo,
    ubicacion: ubicacion
  })
})

/* 
static
  const schema = new Schema(..);
  // Equivalent to `schema.statics.findByName = function(name) {}`;
  schema.static('findByName', function(name) {
    return this.find({ name: name });
  });

  const Drink = mongoose.model('Drink', schema);
  await Drink.findByName('LaCroix');

method
  const schema = kittySchema = new Schema(..);

  schema.method('meow', function () {
    console.log('meeeeeoooooooooooow');
  })

  const Kitty = mongoose.model('Kitty', schema);

  const fizz = new Kitty;
  fizz.meow(); // meeeeeooooooooooooow
*/

bicicletaSchema.method('toString', function() {
  return  'code: ' + this.code + ' | color: ' + this.color
})

// statics significa que estoy trabajado directamente sobre el modelo
bicicletaSchema.static('allBicis', function(callback){
  return this.find({}, callback)
})

bicicletaSchema.static('add', function(aBici, cb){
  return this.create(aBici, cb)
})

bicicletaSchema.static('findBycode', function(aCode, cb){
  return this.findOne({code: aCode}, cb)
})

bicicletaSchema.static('removeByCode', function(aCode, cb){
  return this.deleteOne({code: aCode}, cb)
})


module.exports = model('Bicicleta', bicicletaSchema)