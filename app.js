require('dotenv').config()
var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var session = require('express-session')
const jwt = require('jsonwebtoken');
const MongoDBStore = require('connect-mongodb-session')(session);
require('newrelic');
var passport = require('./config/passport')
const Usuario = require('./models/Usuario')
const Token = require('./models/Token')


require('./database')

let store
// session and cookies
// if(process.env.NODE_ENV === 'development'){
//   // sessiones en memoria
//   store = new session.MemoryStore
// } else {
  store = new MongoDBStore({
    uri: process.env.MONGODB_URI,
    collection: 'sessions'
  })
  store.on('error', function(error){
    assert.ifError(error)
    assert.ok(false)
  })
// }

var app = express();

app.set('secretKey', 'mi_secret_key')

app.use(session({
  cookie: { maxAge: 240 * 60 * 60 * 1000},
  store: store,
  resave: true,
  saveUninitialized: true,
  secret: 'mi secreto'
}))


// RUTAS
  // WEB
  var indexRouter = require('./routes/index');
  var usersRouter = require('./routes/users');
  var bicicletasRouter = require('./routes/bicicletas')
  var tokenRouter = require('./routes/token')
  var usuarioRouter = require('./routes/usuarios')
  // API
  var bicicletaAPIRouter = require('./routes/api/bicicletasAPIRouter')
  var usuarioAPIRouter = require('./routes/api/usuarioAPIRouter')
  var reservaAPIRouter = require('./routes/api/reservasAPIRouter');
  var authApiRouter = require('./routes/api/authApiRouter');
  
  
  const { assert } = require('console');


// MIDDLEWARE
  // view engine setup
  app.set('views', path.join(__dirname, 'views'));
  app.set('view engine', 'pug');

  app.use(logger('dev'));
  app.use(express.json());
  app.use(express.urlencoded({ extended: false }));
  app.use(cookieParser()); // 'Un secreto' 
  app.use(passport.initialize())
  app.use(passport.session())

  app.use(express.static(path.join(__dirname, 'public')));
  
  // WEB
  app.use('/', indexRouter);
  app.use('/users', usersRouter);
  app.use('/bicicletas', bicicletasRouter)
  app.use('/usuarios', loggedIn, usuarioRouter)
  app.use('/token', tokenRouter)
  // API
  app.use('/api/v1/bicicletas', bicicletaAPIRouter)
  app.use('/api/v1/usuarios', usuarioAPIRouter)
  app.use('/api/v1/reservas', reservaAPIRouter)
  app.use('/api/v1/auth', authApiRouter)


  app.use('/privacy_policy', function(req, res){
    res.sendFile('/public/privacy_policy.html')
  })

  //========================================================
  //                       GOOGLE                         
  //========================================================

  app.use('/google2193493386729926', function(req, res){
    res.sendFile('/public/google2193493386729926.html')
  })

app.get(
  '/auth/google',
  passport.authenticate(
    'google',
    { scope: [
      'https://www.googleapis.com/auth/plus.login',
      'https://www.googleapis.com/auth/plus.profile.emails.read'
      ]
    }
  )
);

app.get(
  '/auth/google/callback', 
  passport.authenticate(
    'google',
    { successRedirect: '/', failureRedirect: '/login'}
  ),
  function(req, res) {
    // Successful authentication, redirect home.
    res.redirect('/');
  }
);

  //========================================================
  //                     FACEBOOK                         
  //========================================================
  app.get('/auth/facebook',
  passport.authenticate('facebook'));

  app.get('/auth/facebook/callback',
    passport.authenticate('facebook', { successRedirect: '/', failureRedirect: '/login' }),
    function(req, res) {
      // Successful authentication, redirect home.
      res.redirect('/');
  });

  // login
  app.get('/login', function(req, res){
    res.render('session/login')
  })

  app.post('/login', function(req, res, next){
    passport.authenticate('local', function(err, usuario, info){
      if(err) return next(err)
      if(!usuario) return res.render('session/login', {info})

      req.logIn(usuario, function(err){
        if(err) return next(err)
        return res.redirect('/')
      })
    })(req, res, next)
  })

  // app.post('/login', 
  //   passport.authenticate('local',
  //   {
  //     successRedirect: '/',
  //     failureRedirect: '/login'
  //   })
  // )

  app.get('/logout', function(req, res){
    req.logOut()
    res.redirect('/')
  })

  app.get('/forgotPassword', function(req, res){
    res.render('session/forgotPassword')
  })

  app.post('/forgotPassword', function(req, res, next){
    Usuario.findOne({ email: req.body.email }, function(err, usuario){
      if(!usuario) return res.render('session/forgotPassword', { info: {message: 'El email no esta registrado'}})
      
      usuario.resetPassword(function(err){
        if(err) return next(err)
        console.log('session.forgotPasswordMessage')
      })

      res.render('session/forgotPasswordMessage')
    })
  })

  app.get('/resetPassword/:token', function(req, res, next){
    Token.findOne({ token: req.params.token }, function(err, token){
      if(!token) return res.status(400).send({ type: "no-verified", info: {message: 'No existe un usuario asociado al token. Verifique que su token no haya expirado.'}})

      Usuario.findById(token._userId, function(err, usuario){
        if(!usuario) return res.status(400).send({info: {message: 'No existe un usuario asociado al token.'}})
        res.render('session/resetPassword', {erros: {}, usuario: usuario})
      })
    })
  })

  app.post('/resetPassword', function(req, res){

    if(req.body.password != req.body.confirm_password){
      res.render(
        'session/resetPassword',
        {
          info: {message: 'No coincide con la password ingresada'},
          usuario: new Usuario({email: req.body.email})
        }
      )
      return
    }

    Usuario.findOne({email: req.body.email}, function (err, usuario){
      usuario.password = req.body.password
      usuario.save(function(err){
        if(err){
          res.render('session/resetPassword', { usuario: new Usuario({ email: req.body.email})})
        }else{
          res.redirect('/login')
        }
      })
    })
  })

  // catch 404 and forward to error handler
  app.use(function(req, res, next) {
    next(createError(404));
  });

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

function loggedIn(req, res, next){
  if(req.user){
    next()
  }else{
    console.log('Usuario sin logearse')
    res.redirect('/login')
  }
  // console.log(req.user)
}

function validarUsuario(req, res, next){
  const token = req.headers['x-access-token']
  if(!token){
    return res.status(401).json({
      auth: false,
      message: 'No token provided, Denied access'
    })
  }

  try{
    const decoded = jwt.verify( token, req.app.get('secretKey'))
      req.userId = decoded.id  
      next()
  } catch(err) {
    res.status(400).json( { status: 'error', message: err.message, data: null})
  }
}

// tira error
// function validarUsuario(req, res, next){
//   jwt.verify(req.headers['x-access-token'], req.app.get('secretKey'), function(err, decoded){
//     if(!err){
//       res.json( { status: 'error', message: err.message, data: null})
//     }else{
//       req.body.userId = decoded.id

//       console.log('jwt verify:' + decoded)

//       next()
//     }
//   })
// }

module.exports = app;
