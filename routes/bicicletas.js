const express = require('express');
var router = express.Router()
var BicicletaController = require('../controllers/bicicleta')

router.get('/', BicicletaController.bicicleta_list)
router.get('/create', BicicletaController.bicicleta_create_get)
router.post('/create', BicicletaController.bicicleta_create_post)
router.get('/update/:id', BicicletaController.bicicleta_update_get)
router.post('/update/:id', BicicletaController.bicicleta_update_post)
router.post('/delete/:id', BicicletaController.bicicleta_delete)

module.exports = router;