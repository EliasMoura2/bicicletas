const express = require('express');
const passport = require('passport');
const router = express.Router()
const authApiController = require('../../controllers/api/authAPIController');

router.post('/authenticate', authApiController.authenticate)
router.post('/forgotPassword', authApiController.forgotPassword)
router.post('/facebook_token', passport.authenticate('facebook'), authApiController.authFacebookToken)

module.exports = router