const express = require('express');
var router = express.Router()
var ReservaAPIController = require('../../controllers/api/reservaAPIController')

router.get('/', ReservaAPIController.reservas_list)


module.exports = router;