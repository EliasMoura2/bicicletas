const express = require('express');
const router = express.Router()
var usuariController = require('../../controllers/api/usuarioApiController')

router.get('/', usuariController.usuarios_list)
router.post('/create', usuariController.usuarios_create)
router.post('/reservar', usuariController.usuario_reservar)

module.exports = router