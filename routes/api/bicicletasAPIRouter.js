const express = require('express');
var router = express.Router()
var BicicletaAPIController = require('../../controllers/api/bicicletaAPIController')

router.get('/', BicicletaAPIController.bicicleta_list)
router.get('/show', BicicletaAPIController.bicicleta_show)
router.post('/create', BicicletaAPIController.bicicleta_create)
router.put('/update', BicicletaAPIController.bicicleta_update)
router.delete('/delete', BicicletaAPIController.bicicleta_delete)

module.exports = router;