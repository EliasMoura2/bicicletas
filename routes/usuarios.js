const express = require('express');
const router = express.Router()

var usuarioController = require('../controllers/usuario')

router.get('/', usuarioController.list)
router.get('/create', usuarioController.create_get)
router.post('/create', usuarioController.create_post)
router.get('/update/:id', usuarioController.update_get)
router.post('/update/:id', usuarioController.update_post)
router.post('/delete/:id', usuarioController.delete)

module.exports = router