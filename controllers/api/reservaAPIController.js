const Reserva = require('../../models/Reserva')

exports.reservas_list = function(req, res){
  Reserva.find({}, function(err, reservas){
    res.status(200).json({
      reservas: reservas
    })
  })
}

// en usuario se crean las reservas
