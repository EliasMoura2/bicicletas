const Usuario = require('../../models/Usuario')
const Joi = require('joi')

exports.usuarios_list = function(req, res){
  Usuario.find({}, function(err, usuarios){
    res.status(200).json({
      usuarios: usuarios
    })
  })
}

exports.usuarios_create = function(req, res){
  // Validaciones datos user
  const schemaRegister = Joi.object({
    nombre: Joi.string().min(1).max(45).required(),
    email: Joi.string().min(6).max(255).required().email(),
    password: Joi.string().min(6).max(12).required(),
  })

  // validar datos ingresados
  const { error } = schemaRegister.validate(req.body)

  if( error ){
    return res.status(400).json({ error: error.details[0].message})
  }

  var usuario = new Usuario({
    nombre: req.body.nombre,
    email: req.body.email,
    password: req.body.password
  })

  // console.log(usuario)
  
  // usuario.save(function(err){
  //   res.status(200).json(usuario)
  // })

  usuario.save()

  res.status(200).json({
    usuario: usuario
  })
}

exports.usuario_reservar = function(req, res){
  Usuario.findById(req.body.id, function(err, usuario){
    // console.log(usuario)
    usuario.reservar(req.body.bici_id, req.body.desde, req.body.hasta, function(err){
      // console.log('Reserva !!!')
      res.status(200).send()
    })
  })
}