var Bicicleta = require('../../models/Bicicleta')

exports.bicicleta_list = function (req, res){
  Bicicleta.find({}, function(err, bicicletas){
    res.status(200).json({
      bicicletas: bicicletas
    })
  })
}

exports.bicicleta_create = function(req, res){
  // console.log(req.body)
  var ubicacion = [req.body.lat, req.body.long]
  var bici = new Bicicleta({
    code: req.body.code,
    color: req.body.color,
    modelo: req.body.modelo,
    ubicacion: ubicacion
  })
  // console.log(bici)
  bici.save()
  res.status(200).json({
    bicicleta: bici
  })
}

exports.bicicleta_update = async function(req, res){    

  var datosActualizados = {
    code: req.body.code,
    color: req.body.color,
    modelo: req.body.modelo,
    ubicacion: {
      lat: req.body.lat,
      long: req.body.long
    }
  }

  const query = {code: req.body.code}

  await Bicicleta.update(query, datosActualizados);
  
  var bici = await Bicicleta.findOne({code: req.body.code})  

  res.status(200).json({
    bicicleta: bici
  })
}

exports.bicicleta_delete = async function(req, res){
  await Bicicleta.deleteOne({code: req.body.code})
  res.status(204).send()
}

exports.bicicleta_show = function(req, res){
  Bicicleta.findById(req.body.id, function(err, bicicleta){
    bicicleta.toString()
  })
}