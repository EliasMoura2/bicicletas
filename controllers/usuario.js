var Usuario = require('../models/Usuario')

module.exports={
  list: function(req, res, next){
    Usuario.find({}, (err, usuarios) => {
      res.render('usuarios/index', { usuarios: usuarios })
    })
  },
  create_get: function(req, res, next){
    res.render('usuarios/create', {errors: {}, usuario: new Usuario()})
  },
  create_post: function(req, res, next){
    if(req.body.password != req.body.confir_pass){
      res.render('usuarios/create', {errors: {confirm_pass: {message: 'Las passwords no coinciden'}}, usuario: new Usuario({nombre: req.body.nombre, email: req.body.email})})
      return
    }

    Usuario.create({nombre: req.body.nombre, email: req.body.email, password: req.body.password}, (err, nuevoUsuario) => {
      if(err){
        res.render('usuarios/create', {errors: err.errors, usuario: new Usuario({nombre: req.body.nombre, email: req.body.email})})
      }else{
        nuevoUsuario.enviar_email_bienvenida()
        res.redirect('/usuarios')
      }
    })
  },
  update_get: function(req, res, next){
    Usuario.findById(req.params.id, (err, usuario) => {
      res.render('usuarios/update', {errors: {}, usuario: usuario})
    })
  },
  update_post: function(req, res, next){
    // Se actualiza solo el nomobre
    var update_values = {nombre: req.body.nombre}
    Usuario.findByIdAndUpdate(req.params.id, update_values, (err, usuario) => {
      if(err){
        console.log(err)
        res.render('usuarios/update', {errors: err.errors, usuario: new Usuario({nombre: req.body.nombre, email: req.body.email})})
      }else{
        res.redirect("/usuarios")
        return
      }
    })
  }, 
  delete: function(req, res, next){
    Usuario.findByIdAndDelete(req.body.id, (err) =>{
      if(err)
        next(err)
      else
        res.redirect('/usuarios')
    })
  }
}