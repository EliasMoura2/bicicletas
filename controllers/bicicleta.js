// var Bicicleta = require('../models/BicicletaMemoria')
var Bicicleta = require('../models/Bicicleta')


module.exports={
  bicicleta_list: function(req, res, next){
      // exports.bicicleta_list = function(req, res){
      //   res.render('bicicletas/index', { bicis: Bicicleta.allBicis})
      // }
    Bicicleta.find({}, (err, bicicletas) =>{
      res.render('bicicletas/index', {bicis: bicicletas})
    })
  },
  bicicleta_create_get: function(req, res, next){
    // exports.bicicleta_create_get = function(req, res){
    //   // metodo para acceder a la pagina de creacion
    //   res.render('bicicletas/create')
    // }
    res.render('bicicletas/create', {errors: {}, bicicleta: new Bicicleta()})
  },
  bicicleta_create_post: function(req, res, next){
    // exports.bicicleta_create_post = function(req, res){
    //   // metodo para el post de crear bicicletas
    //   var bici = new Bicicleta(req.body.id, req.body.color, req.body.modelo)
    //   bici.ubicacion = [req.body.lat, req.body.long]
    //   Bicicleta.add(bici)
    //   res.redirect('/bicicletas')
    // }

    var ubicacion = [req.body.lat, req.body.long]
    var aBici = {code: req.body.id, color: req.body.color, modelo: req.body.modelo, ubicacion: ubicacion}
    // console.log(aBici)
    Bicicleta.create( aBici, (err, nuevaBici) => {
      if(err){
        res.render('bicicletas/create', {errors: err.errors, bicicleta: new Bicicleta({code: req.body.id, color: req.body.color, modelo: req.body.modelo, ubicacion: [req.body.lat, req.body.long]})})
      }else{
        res.redirect('/bicicletas')
      }
    })
  },
  bicicleta_update_get: function(req, res, next){
    // exports.bicicleta_update_get = function(req, res){
    //   var bici = Bicicleta.findById(req.params.id)
    //   res.render('bicicletas/update', { bici })
    // }
    Bicicleta.findById(req.params.id, (err, bicicleta) => {
      res.render('bicicletas/update', {errors: {}, bici: bicicleta})
    })
  },
  bicicleta_update_post: function(req, res, next){
    // exports.bicicleta_update_post = function(req, res){
    //   var bici = Bicicleta.findById(req.params.id)
    //   bici.id  = req.body.id
    //   bici.color = req.body.color
    //   bici.modelo = req.body.modelo
    //   bici.ubicacion = [req.body.lat, req.body.long]
    //   res.redirect('/bicicletas') 
    // }

    var ubicacion = [req.body.lat, req.body.long]
    var update_values = {code: req.body.id, color: req.body.color, modelo: req.body.modelo, ubicacion: ubicacion}

    Bicicleta.findByIdAndUpdate(req.params.id, update_values, (err, bicicleta) => {
      if(err){
        console.log(err)
        res.render('bicicletas/update', {errors: err.errors, bicicleta: new Bicicleta({code: req.body.id, color: req.body.color, modelo: req.body.modelo, ubicacion: [req.body.lat, req.body.long]})})
      }else{
        res.redirect("/bicicletas")
        return
      }
    })
  },
  bicicleta_delete: function(req, res, next){
    // exports.bicicleta_delete = function(req, res){
    //   Bicicleta.removeById(req.body.id)
    //   res.redirect('/bicicletas')
    // }
    Bicicleta.findByIdAndDelete(req.body.id, (err) =>{
      if(err)
        next(err)
      else
        res.redirect('/bicicletas')
    })
  }
}
