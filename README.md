# Coursera Red_Bicicletas
Proyecto creado en base al tutorial brindado en la plataforma Coursera por la Universidad Austral.
Docente: **Ezequiel Lamónica**

## Project setup
```
npm install
```

### Correr el proyecto en Desarrollo
```
npm run devstart
```



### bitbucket: 
https://bitbucket.org/EliasMoura2/bicicletas/src/master/

### github: 
https://github.com/EliasMoura2 


#### Endpoints al correr de manera local
##### get
http://localhost:3000/api/v1/bicicletas
##### post
http://localhost:3000/api/v1/bicicletas/create
```
{
    "id": 3,
    "color": "blanco",
    "modelo": "Urbano",
    "lat": -27.367990,
    "long": -55.901467
}
```
##### put
http://localhost:3000/api/v1/bicicletas/update
```
{
    "id": 3,
    "color": "Rojo",
    "modelo": "Carrera",
    "lat": -27.367990,
    "long": -55.901467
}
```
##### delete
http://localhost:3000/api/v1/bicicletas/delete
```
{
    "id": 2
}
```