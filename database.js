const mongoose = require('mongoose');

// si estamos en el ambiente de desarrollo usar la bd local
// si estamos en el ambiente de produccion usar la bd de produccion


mongoose.connect(process.env.MONGODB_URI, {
    useNewUrlParser: true,
    useUnifiedTopology: true
  })
  .then( (db) => console.log(`DB is CONNECTED on ${process.env.MONGODB_URI}`))
  .catch((err) => console.error(err))

  // mongoose.connect(URI, {
//   useNewUrlParser: true,
//   useUnifiedTopology: true
// })

// // configuracion
// mongoose.Promise = global.Promise

// // guardamos la coneccion
// const db = mongoose.connection

// db.on('error', console.error.bind(console, 'Mongo connection error'))

/* 
  // The connect() function also accepts a callback parameter and returns a promise.

  mongoose.connect(uri, options, function(error) {
    // Check error in initial connection. There is no 2nd param to the callback.
  });

  // Or using promises
  mongoose.connect(uri, options).then(
    () => { /** ready to use. The `mongoose.connect()` promise resolves to mongoose instance. / },
    err => { //** handle initial connection error / }
  );

  .catch(err => console.log(err.reason))
*/